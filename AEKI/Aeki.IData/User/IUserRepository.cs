using System.Threading.Tasks;

namespace Aeki.IData.User
{
    public interface IUserRepository
    {
        Task<int> AddUser(Aeki.Domain.User.User user);
        Task<Aeki.Domain.User.User> GetUser(int userId);
        Task<Aeki.Domain.User.User> GetUser(string userName);
        Task EditUser(Domain.User.User user);
 
    }
}