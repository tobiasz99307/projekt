using System.Threading.Tasks;

namespace Aeki.IData.Category
{
    public interface ICategoryRepository
    {
        Task<int> AddCategory(Aeki.Domain.Category.Category category);
        Task<Aeki.Domain.Category.Category> GetCategory(int categoryId);
        Task<Aeki.Domain.Category.Category> GetCategory(string categoryName);
        Task EditCategory(Domain.Category.Category category);
    }
}