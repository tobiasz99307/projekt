using Aeki.IServices.Category;
using System.Threading.Tasks;
using Aeki.IData.Category;
using Aeki.IServices.Requests;

namespace Aeki.Services.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        { 
            _categoryRepository= categoryRepository;
        }

        public Task<Domain.Category.Category> GetCategoryByCategoryId(int catergoryId)
        {
            return _categoryRepository.GetCategory(catergoryId);
        }

        public Task<Domain.Category.Category> GetCategoryByName(string categoryName)
        {
            return _categoryRepository.GetCategory(categoryName);
        }

        public async Task<Domain.Category.Category> CreateCategory(CreateCategory createCategory)
        {
            var category = new Domain.Category.Category(createCategory.CategoryName,createCategory.CategoryImgLink);
            category.Id = await _categoryRepository.AddCategory(category);
            return category;
        }

        public async Task EditCategory(EditCategory createCategory, int categoryId)
        {
            var category = await _categoryRepository.GetCategory(categoryId);
            category.EditCategory(createCategory.CategoryName, createCategory.CategoryImgLink);
            await _categoryRepository.EditCategory(category);
        }
    }
}