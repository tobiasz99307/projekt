using System.Threading.Tasks;
using Aeki.IData.User;
using Aeki.IServices.Requests;
using Aeki.IServices.User;

namespace Aeki.Services.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<Domain.User.User> GetUserByUserId(int userId)
        {
            return _userRepository.GetUser(userId);
        }

        public Task<Domain.User.User> GetUserByUserMail(string userMail)
        {
            return _userRepository.GetUser(userMail);
        }

        public async Task<Domain.User.User> CreateUser(CreateUser createUser)
        {
            var user = new Domain.User.User(createUser.UserName, createUser.UserLName,createUser.Email, createUser.Phone);
            user.Id = await _userRepository.AddUser(user);
            return user;
        }

        public async Task EditUser(EditUser createUser, int userId)
        {
            var user = await _userRepository.GetUser(userId);
            user.EditUser(createUser.UserName, createUser.UserLName,createUser.Email, createUser.Phone, createUser.Verified, createUser.UserCommentBanned);
            await _userRepository.EditUser(user);
        }

    }
}