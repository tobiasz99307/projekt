using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Aeki.Api.BindingModels;
using Aeki.Api.HealthChecks;
using Aeki.Api.Middlewares;
using Aeki.Data.Sql;
using Aeki.Data.Sql.Category;
using Aeki.Data.Sql.Migrations;
using Aeki.Data.Sql.User;
using Aeki.IData.Category;
using Aeki.IData.User;
using Aeki.IServices.Category;
using Aeki.IServices.User;
using Aeki.Services.Category;
using Aeki.Services.User;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Aeki.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private const string MySqlHealthCheckName = "mysql";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //Póki co nieobowiązkowa treść, ale warto się zapoznać
        //metoda odpowiadająca za rejestrację serwisów w kontenerze IoC
        //zanim przejdą Państwo do kontenerów Dependency Injection (wstrzykwiania zależności)
        //warto zapoznać się z SOLIDem
        //SOLID - http://www.pzielinski.com/?s=ZASADY+S.O.L.I.D
        //oraz z Inversion of Control (link zawiera również wyjaśnienie Dependency Injection
        //https://www.c-sharpcorner.com/UploadFile/cda5ba/dependency-injection-di-and-inversion-of-control-ioc/
        public void ConfigureServices(IServiceCollection services)
        {
            //rejestracja DbContextu, użycie providera MySQL i pobranie danych o bazie z appsettings.json
            services.AddDbContext<AekiDbContext>(options => options
                .UseMySQL(Configuration.GetConnectionString("AekiDbContext")));
            services.AddTransient<DatabaseSeed>();
            services.AddHealthChecks()
                .AddMySql(
                    Configuration.GetConnectionString("AekiDbServer"),
                    4,
                    10,
                    MySqlHealthCheckName);
            services.AddControllers().AddNewtonsoftJson(options => {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation();
            services.AddTransient<IValidator<EditUser>, EditUserValidator>();
            services.AddApiVersioning( o => o.ReportApiVersions = true );
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();

            services.AddApiVersioning( o =>
            {
                o.ReportApiVersions = true;
                o.UseApiBehavior = false;
            });

        }


        // Metoda w której konfiguruje się pipeline (potok) żądań HTTP.
        //Ja użyłem tej metody do upewnienia się, że baza którą chce utworzyć nie istnieje,
        //a następnie do utworzenia bazy danych i utworzenia testowych danych

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<AekiDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                //sprawdzenie czy health check się powiódł
                var healthCheck = serviceScope.ServiceProvider.GetRequiredService<HealthCheckService>();
                if (healthCheck.CheckHealthAsync().Result?.Entries[MySqlHealthCheckName].Status
                    == HealthCheckResult.Healthy().Status)
                {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                    databaseSeed.Seed();
                }
            }
            
            app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}