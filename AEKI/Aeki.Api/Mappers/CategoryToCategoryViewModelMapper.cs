using Aeki.Api.ViewModels;

namespace Aeki.Api.Mappers
{
    public class CategoryToCategoryViewModelMapper
    {
        public static CategoryViewModel CategoryToCategoryViewModel(Domain.Category.Category category)
        {
            var categoryViewModel = new CategoryViewModel()
            {
                CategoryId = category.Id,
                CategoryName = category.CategoryName,
                CategoryImgLink = category.CategoryImgLink
            };
            
            return categoryViewModel;
        }
       
    }
}