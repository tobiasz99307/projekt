using Aeki.Api.ViewModels;

namespace Aeki.Api.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel
            {
                UserId = user.Id,
                UserName = user.UserFName,
                UserSurName = user.UserLName,
                Email = user.Email,
                Phone = user.Phone,
                Verified = user.Verified,
                UserCommentBanned = user.UserCommentBanned,
           };
            return userViewModel;
        }
 
    }
}