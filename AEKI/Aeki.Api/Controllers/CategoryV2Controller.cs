using System.Threading.Tasks;
using Aeki.Api.Mappers;
using Aeki.Api.Validation;
using Aeki.Data.Sql;
using Aeki.IServices.Category;
using Microsoft.AspNetCore.Mvc;

namespace Aeki.Api.Controllers
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/category")]
    public class CategoryV2Controller : Controller
    {
        private readonly AekiDbContext _context;
        private readonly ICategoryService _categoryService;

        /// <inheritdoc />
        public CategoryV2Controller(AekiDbContext context, ICategoryService categoryService)
        {
            _context = context;
            _categoryService = categoryService;
        }

        [HttpGet("{categoryId:min(1)}", Name = "GetCategoryById")]
        public async Task<IActionResult> GetCategoryByCategoryId(int categoryId)
        {
            var category = await _categoryService.GetCategoryByCategoryId(categoryId);
            if (category != null)
            {
                return Ok(CategoryToCategoryViewModelMapper.CategoryToCategoryViewModel(category));
            }

            return NotFound();
        }

        [HttpGet("name/{categoryName}", Name = "GetCategoryByCategoryName")]
        public async Task<IActionResult> GetCategoryByName(string categoryName)
        {
            var category = await _categoryService.GetCategoryByName(categoryName);
            if (category != null)
            {
                return Ok(CategoryToCategoryViewModelMapper.CategoryToCategoryViewModel(category));
            }

            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] IServices.Requests.CreateCategory createCategory)
        {
            var category = await _categoryService.CreateCategory(createCategory);

            return Created(category.Id.ToString(), CategoryToCategoryViewModelMapper.CategoryToCategoryViewModel(category));
        }


        [ValidateModel]
        [HttpPatch("edit/{categoryId:min(1)}", Name = "EditCategory")]
//        public async Task<IActionResult> EditCategory([FromBody] EditCategory editCategory,[FromQuery] int categoryId)
        public async Task<IActionResult> EditCategory([FromBody] IServices.Requests.EditCategory editCategory, int categoryId)
        {
            await _categoryService.EditCategory(editCategory, categoryId);

            return NoContent();
        }
    }
}