using System;
using System.Linq;
using System.Threading.Tasks;
using Aeki.Api.BindingModels;
using Aeki.Api.Validation;
using Aeki.Api.ViewModels;
using Aeki.Data.Sql;
using Aeki.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Aeki.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class UserController : Controller
    {
        private readonly AekiDbContext _context;

        public UserController(AekiDbContext context)
        {
            _context = context;
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    UserName = user.UserFName,
                    UserSurName = user.UserLName,
                    Email = user.UserMail,
                    Phone = user.UserPhone,
                    Verified = user.UserVerified,
                    UserCommentBanned = user.UserCommentBanner,
                });
            }

            return NotFound();
        }

        [HttpGet("mail/{userMail}", Name = "GetUserByUserMail")]
        public async Task<IActionResult> GetUserByUserName(string userMail)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserMail == userMail);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    UserName = user.UserFName,
                    UserSurName = user.UserLName,
                    Email = user.UserMail,
                    Phone = user.UserPhone,
                    Verified = user.UserVerified,
                    UserCommentBanned = user.UserCommentBanner,
                });
            }

            return NotFound();
        }
       
        [ValidateModel]
//        [Consumes("application/x-www-form-urlencoded")]
//        [HttpPost("create", Name = "CreateUser")]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User
            {
                UserFName = createUser.UserFName,
                UserLName = createUser.UserLName,
                UserMail = createUser.Email,
                UserPhone = createUser.Phone,
                UserVerified = false,
                UserCommentBanner = false
            };
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return Created(user.UserId.ToString(), new UserViewModel
            {
                    UserId = user.UserId,
                    UserName = user.UserFName,
                    UserSurName = user.UserLName,
                    Email = user.UserMail,
                    Phone = user.UserPhone,
                    Verified = user.UserVerified,
                    UserCommentBanned = user.UserCommentBanner,
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
//        public async Task<IActionResult> EditUser([FromBody] EditUser editUser,[FromQuery] int userId)
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            user.UserFName = editUser.UserFName;
            user.UserLName = editUser.UserLName;
            user.UserMail = editUser.Email;
            user.UserPhone = editUser.Phone;
            user.UserVerified = editUser.Verifed;
            await _context.SaveChangesAsync();
           // return NoContent();
            return Ok(new UserViewModel
            {
                    UserId = user.UserId,
                    UserName = user.UserFName,
                    UserSurName = user.UserLName,
                    Email = user.UserMail,
                    Phone = user.UserPhone,
                    Verified = user.UserVerified,
                    UserCommentBanned = user.UserCommentBanner,
            });
        }

        [ValidateModel]
        [HttpDelete("delete/{userId:min(1)}", Name = "DeleteUserById")]
        public async Task<IActionResult> DeleteUserById(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            var ratings = await _context.Rating.Where(x => x.UserId == userId).ToListAsync();
            var comments= await _context.Comment.Where(x => x.UserId == userId).ToListAsync();
            var orders= await _context.Order.Where(x => x.UserId == userId).ToListAsync();
            if (user == null) return NotFound();

            //deleting constrains
            foreach (var rating in ratings)
            {
                _context.Rating.Remove(rating);
            }
            
            foreach (var comment in comments)
            {
                _context.Comment.Remove(comment);
            }
            
            foreach (var order in orders)
            {
                _context.Order.Remove(order);
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return Ok();
        }
        
        
    }
}