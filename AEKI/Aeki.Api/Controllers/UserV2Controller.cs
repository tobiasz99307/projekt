using System.Threading.Tasks;
using Aeki.Api.Mappers;
using Aeki.Api.Validation;
using Aeki.Data.Sql;
using Aeki.IServices.User;
using Microsoft.AspNetCore.Mvc;

namespace Aeki.Api.Controllers
{    
    [ApiVersion( "2.0" )]
    [Route( "api/v{version:apiVersion}/user" )]

    public class UserV2Controller: Controller
    {
        
        private readonly AekiDbContext _context;
        private readonly IUserService _userService;

        /// <inheritdoc />
        public UserV2Controller(AekiDbContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }
        
        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userService.GetUserByUserId(userId);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }
            return NotFound();
        }
        
        [HttpGet("mail/{userMail}", Name = "GetUserByUserMail")]
        public async Task<IActionResult> GetUserByUserMail(string userMail)
        {
            var user = await _userService.GetUserByUserMail(userMail);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }
            return NotFound();
        }
        
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] IServices.Requests.CreateUser createUser)
        {
            var user = await _userService.CreateUser(createUser);
            
            return Created(user.Id.ToString(),UserToUserViewModelMapper.UserToUserViewModel(user)) ;
        }
        
        
        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
//        public async Task<IActionResult> EditUser([FromBody] EditUser editUser,[FromQuery] int userId)
        public async Task<IActionResult> EditUser([FromBody] IServices.Requests.EditUser editUser, int userId)
        {
            await _userService.EditUser(editUser, userId);
            
            return NoContent();
        }
    }
}