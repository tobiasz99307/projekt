using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace Aeki.Api.BindingModels
{
    public class EditUser
    {
        
        [Display(Name = "UserName")]
        public string UserFName { get; set; }
    
        [Display(Name= "UserSurname")]
        public string UserLName { get; set; }
        
        [Display(Name = "Email")]
        public string Email { get; set; }
    
        public string Phone { get; set; }
    
        [Display(Name = "Verified")]
        public bool Verifed { get; set; }

        [Display(Name = "BanFromComments")]
        public bool BannedComments { get; set; }
    }

    public class EditUserValidator : AbstractValidator<EditUser>
    {
        public EditUserValidator()
        {
            RuleFor(x => x.UserFName).NotNull();
            RuleFor(x => x.UserLName).NotNull();
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Verifed).NotNull();
            RuleFor(x => x.BannedComments).NotNull();
        }
    }
}