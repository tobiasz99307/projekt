using System;
using System.ComponentModel.DataAnnotations;

namespace Aeki.Api.BindingModels
{
    public class CreateUser
    {
        //TODO: F name L name
        [Required]
        [Display(Name = "UserName")]
        public string UserFirstName { get; set; }
    
        [Required]
        [Display(Name= "UserSurname")]
        public string UserLastName { get; set; }
        
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
    
        [Required]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
    
        [Required]
        [Display(Name = "Verified")]
        public bool Verifed { get; set; }

        [Required]
        [Display(Name = "BanFromComments")]
        public bool BannedComments { get; set; }
    }
}
