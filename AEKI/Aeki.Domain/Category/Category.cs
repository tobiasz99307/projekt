namespace Aeki.Domain.Category
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryImgLink { get; set; }

        public Category(int categoryId, string categoryName, string categoryImgLink)
        {
            Id = categoryId;
            CategoryName = categoryName;
            CategoryImgLink = categoryImgLink;
        }
        public Category(string categoryName, string categoryImgLink)
        {
            CategoryName = categoryName;
            CategoryImgLink = categoryImgLink;
        }
        
        public void EditCategory(string categoryName, string categoryImgLink)
        {
            CategoryName = categoryName;
            CategoryImgLink = categoryImgLink;
        }
    }
}
