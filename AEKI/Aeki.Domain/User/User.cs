using System;

namespace Aeki.Domain.User
{
    public class User
    {
       public int Id { get; set; }
        public string UserFName { get; private set; }
        public string UserLName { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public bool Verified { get; private set; }
        public bool UserCommentBanned { get; private set; }
        public User(int id, string userFName, string userLName, string email, string phone, bool verified, bool userCommentBanned ) 
        {
            Id = id;
            UserFName = userFName;
            UserLName = userLName;
            Email = email;
            Phone = phone;
            Verified = verified;
            UserCommentBanned = userCommentBanned;
        }
        public User(string userFName,string userLName, string email, string phone) 
        {
            UserFName = userFName;
            UserLName = userLName;
            Email = email;
            Phone = phone;
            Verified = false;
            UserCommentBanned = false;
        }
        
        public void EditUser(string userFName, string userLName, string email, string phone, bool verified, bool userCommentBanned ) 
        {
            UserFName = userFName;
            UserLName = userLName;
            Email = email;
            Phone = phone;
            Verified = verified;
            UserCommentBanned = userCommentBanned;
        }

    }
}