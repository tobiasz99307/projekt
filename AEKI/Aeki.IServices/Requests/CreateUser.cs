namespace Aeki.IServices.Requests
{
    public class CreateUser
    {
        public string UserName { get; set; }
        
        public string UserLName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
