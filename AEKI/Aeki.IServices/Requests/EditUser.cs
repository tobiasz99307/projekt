namespace Aeki.IServices.Requests
{
    public class EditUser
    {
        public string UserName { get; set; }
        
        public string UserLName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool Verified { get; set; }
        public bool UserCommentBanned { get; set; }
 
    }
}