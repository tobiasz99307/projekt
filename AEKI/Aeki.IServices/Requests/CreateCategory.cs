namespace Aeki.IServices.Requests
{
    public class CreateCategory
    {
        public string CategoryName { get; set; }
        public string CategoryImgLink { get; set; }
    }
}
