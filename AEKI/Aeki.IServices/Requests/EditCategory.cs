namespace Aeki.IServices.Requests
{
    public class EditCategory
    {
        public string CategoryName { get; set; }
        public string CategoryImgLink { get; set; }
    }
}
