using System.Threading.Tasks;
using Aeki.IServices.Requests;

namespace Aeki.IServices.Category
{
    public interface ICategoryService
    {
         Task<Aeki.Domain.Category.Category> GetCategoryByCategoryId(int categoryId);
         Task<Aeki.Domain.Category.Category> GetCategoryByName(string categoryName);
         Task<Aeki.Domain.Category.Category> CreateCategory(CreateCategory createCategory);
         Task EditCategory(EditCategory createCategory, int categoryId);
    }
}