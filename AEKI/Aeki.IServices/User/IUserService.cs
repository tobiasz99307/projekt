using System.Threading.Tasks;
using Aeki.IServices.Requests;

namespace Aeki.IServices.User
{
    public interface IUserService
    {
        Task<Aeki.Domain.User.User> GetUserByUserId(int userId);
        Task<Aeki.Domain.User.User> GetUserByUserMail(string userMail);
        Task<Aeki.Domain.User.User> CreateUser(CreateUser createUser);
        Task EditUser(EditUser createUser, int userId);
    }
}