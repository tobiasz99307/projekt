﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class ManufacturerConfiguration : IEntityTypeConfiguration<Manufacturer>
    {
        public void Configure(EntityTypeBuilder<Manufacturer> builder)
        {
            builder.Property(w => w.ManufacturerName).IsRequired();
            builder.Property(w => w.ManufacturerId).IsRequired();

            builder.ToTable("Manufacturer");
        } 
    }
}