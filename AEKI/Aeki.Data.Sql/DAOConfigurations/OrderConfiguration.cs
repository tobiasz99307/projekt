﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(o => o.OrderDate).IsRequired();
            builder.Property(o => o.OrderDestAdress).IsRequired();
            builder.Property(o => o.OrderPayMethod).IsRequired();

            builder.HasOne(u => u.User)
                .WithMany(o => o.Orders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(o => o.UserId);
;
        }
    }
}