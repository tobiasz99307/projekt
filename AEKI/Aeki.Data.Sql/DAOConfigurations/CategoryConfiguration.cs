﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<DAO.Category>
    {
        public void Configure(EntityTypeBuilder<DAO.Category> builder)
        {
            builder.Property(c => c.CategoryName).IsRequired();
            builder.Property(c => c.CategoryImgLink).IsRequired();
            
            builder.ToTable("Category");
        }
    }
}