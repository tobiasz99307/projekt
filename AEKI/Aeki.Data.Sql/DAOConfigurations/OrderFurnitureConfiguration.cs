﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class OrderFurnitureConfiguration : IEntityTypeConfiguration<OrderFurniture>
    {
        public void Configure(EntityTypeBuilder<OrderFurniture> builder)
        {
            builder.HasOne(x => x.Order)
                .WithMany(x => x.Orders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OrderId);
            
            builder.HasOne(x => x.Furniture)
                .WithMany(x =>x.OrderFurnitures)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FurnitureId);
            builder.ToTable("OrderFurniture");
        }
    }
}