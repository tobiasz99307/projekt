﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class RatingConfiguration :  IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.Property(r => r.RatingValue).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Ratings)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            

            builder.HasOne(x => x.Furniture)
                .WithMany(x => x.Ratings)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FurnitureId);
            builder.ToTable("Rating");
        } 
    }
}