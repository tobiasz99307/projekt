﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.Property(c => c.CommentContent).IsRequired();

            builder.Property(c => c.CommentDate);

            builder.HasOne(x => x.Furniture)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FurnitureId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Comment");
        }
    }
}