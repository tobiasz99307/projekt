﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<DAO.User>
    {
        public void Configure(EntityTypeBuilder<DAO.User> builder)
        {
            builder.Property(u => u.UserFName).IsRequired();
            builder.Property(u => u.UserLName).IsRequired();
            builder.Property(u => u.UserMail).IsRequired();
            builder.Property(u => u.UserCommentBanner).IsRequired().HasColumnType("tinyint(1)");
            builder.Property(u => u.UserVerified).HasColumnType("tinyint(1)");

            builder.ToTable("User");
        }  
    }
}