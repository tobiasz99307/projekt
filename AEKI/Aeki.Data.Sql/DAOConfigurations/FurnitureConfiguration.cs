﻿using Aeki.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Aeki.Data.Sql.DAOConfigurations
{
    public class FurnitureConfiguration : IEntityTypeConfiguration<Furniture>
    {
        public void Configure(EntityTypeBuilder<Furniture> builder)
        {
            builder.Property(f => f.FurnitureName).IsRequired();
            builder.Property(f => f.FurnitureCost).IsRequired();
            builder.Property(f => f.FurnitureQuantity).IsRequired();
            builder.Property(f => f.FurnitureMaterial).IsRequired();

            builder.HasOne(x => x.Category)
                .WithMany(c => c.Furnitures)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);
      
            builder.HasOne(x => x.Manufacturer)
                .WithMany(c => c.Furnitures)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ManufacturerId);
            builder.ToTable("Furniture");
        } 
    } 
}