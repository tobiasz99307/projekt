﻿using Aeki.Data.Sql.DAO;
using Aeki.Data.Sql.DAOConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Aeki.Data.Sql
{
    public class AekiDbContext : DbContext
    {
        public AekiDbContext(DbContextOptions<AekiDbContext> options) : base(options)
        {
        }

        public virtual DbSet<DAO.Category> Category { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Manufacturer> Manufacturer { get; set; }
        public virtual DbSet<Furniture> Furniture { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderFurniture> OrderFurniture { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<DAO.User> User { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CategoryConfiguration());
            builder.ApplyConfiguration(new CommentConfiguration());
            builder.ApplyConfiguration(new FurnitureConfiguration());
            builder.ApplyConfiguration(new ManufacturerConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new OrderFurnitureConfiguration());
            builder.ApplyConfiguration(new RatingConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
        }
    }
}