using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using Aeki.Data.Sql.DAO;
using Aeki.Data.Sql.Extensions;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Crypto.Engines;

namespace Aeki.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly AekiDbContext _context;

        //wstrzyknięcie instancji klasy FoodlyDbContext poprzez konstruktor
        public DatabaseSeed(AekiDbContext context)
        {
            _context = context;
        }

        //metoda odpowiadająca za uzupełnienie utworzonej bazy danych testowymi danymi
        //kolejność wywołania ma niestety znaczenie, ponieważ nie da się utworzyć rekordu
        //w bazie dnaych bez znajmości wartości klucza obcego
        //dlatego należy zacząć od uzupełniania tabel, które nie posiadają kluczy obcych
        //--OFFTOP
        //w przeciwną stronę działa ręczne usuwanie tabel z wypełnionymi danymi w bazie danych
        //należy zacząć od tabel, które posiadają klucze obce, a skończyć na tabelach, które 
        //nie posiadają
        public void Seed()
        {
            //regiony pozwalają na zwinięcie kodu w IDE
            //nie sa dobrą praktyką, kod w danej klasie/metodzie nie powinien wymagać jego zwijania
            //zastosowałem je z lenistwa nie powinno to mieć miejsca 

            #region CreateUsers

            var userList = BuildUserList();
            //dodanie użytkowników do tabeli User
            _context.User.AddRange(userList);
            //zapisanie zmian w bazie danych
            _context.SaveChanges();

            #endregion

            #region CreateCategories

            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();

            #endregion

            #region CreateManufacturers

            var manufacturerList = BuildManufacturerList();
            _context.Manufacturer.AddRange(manufacturerList);
            _context.SaveChanges();

            #endregion

            #region CreateFurniture

            var furnitureList = BuildFurnitureList(categoryList,manufacturerList);
            _context.Furniture.AddRange(furnitureList);
            _context.SaveChanges();

            #endregion

            #region CreateComments
            
            var commentList = BuildCommentList(furnitureList, userList);
            _context.Comment.AddRange(commentList);
            _context.SaveChanges();
            
            #endregion

            #region CrateRatings

            var ratingList = BuildRatingList(furnitureList, userList);
            _context.Rating.AddRange(ratingList);
            _context.SaveChanges();

            #endregion

            #region CreateOrder

            var orderList = BuildOrderList(userList);
            _context.Order.AddRange(orderList);
            _context.SaveChanges();

            #endregion

            #region CreateOrderFurniture
            
            var orderFurnitureList = BuildOrderFurnitureList(furnitureList, orderList);
            _context.OrderFurniture.AddRange(orderFurnitureList);
            _context.SaveChanges();
            
            #endregion

        }

        private IEnumerable<DAO.User> BuildUserList()
        {
            var userList = new List<DAO.User>();
            var user = new DAO.User()
            {
                UserFName = "Tobiasz",
                UserLName = "Mateja",
                UserMail = "t.mateja@student.po.edu.pl",
                UserPhone = "123 123 123",
                UserVerified = true,
                UserCommentBanner = false,
            };
            userList.Add(user);

            var user2 = new DAO.User()
            {
                UserFName = "Jan",
                UserLName = "Kowaslki",
                UserMail = "kowalski@email.pl",
                UserPhone = "123 123 123",
                UserVerified = true,
                UserCommentBanner = true,
            };
            userList.Add(user2);

            for (int i = 3; i <= 20; i++)
            {
                var user3 = new DAO.User
                {
                    UserFName = "user" + i,
                    UserLName = "user" + i,
                    UserMail = "user" + i + "student.po.edu.pl",
                    UserPhone = "123 123 123",
                    UserVerified = true,
                    UserCommentBanner = false,
                };
                userList.Add(user3);
            }

            return userList;
        }

        private IEnumerable<DAO.Category> BuildCategoryList()
        {
            var categoryList = new List<DAO.Category>
            {
                new DAO.Category
                {
                    CategoryName = "Łazienka",
                    CategoryImgLink = "https://cdn.pixabay.com/photo/2017/02/24/12/23/bathroom-2094716_960_720.jpg",
                },
                new DAO.Category
                {
                    CategoryName = "Kuchnia",
                    CategoryImgLink = "https://cdn.pixabay.com/photo/2017/03/22/17/39/kitchen-2165756_960_720.jpg",
                },
                new DAO.Category
                {
                    CategoryName = "Salon",
                    CategoryImgLink = "https://cdn.pixabay.com/photo/2016/11/18/17/20/living-room-1835923_960_720.jpg",
                },
                new DAO.Category
                {
                    CategoryName = "Sypialnia",
                    CategoryImgLink = "https://cdn.pixabay.com/photo/2014/07/10/17/17/bedroom-389254_960_720.jpg",
                },
                new DAO.Category
                {
                    CategoryName = "Ogród",
                    CategoryImgLink = "https://cdn.pixabay.com/photo/2015/06/12/12/10/castle-park-806854_960_720.jpg",
                },
            };
            return categoryList;
        }

        private IEnumerable<Manufacturer> BuildManufacturerList()
        {
            var manufacturerList = new List<Manufacturer>
            {
                new Manufacturer
                {
                    ManufacturerName = "Black Red White",
                },
                new Manufacturer
                {
                    ManufacturerName = "Helvetia",
                },
                new Manufacturer
                {
                    ManufacturerName = "Klose",
                },
                new Manufacturer
                {
                    ManufacturerName = "Pinio",
                },
            };
            return manufacturerList;
        }

        private IEnumerable<Furniture> BuildFurnitureList(IEnumerable<DAO.Category> listCategory,IEnumerable<Manufacturer> listManufacturer)
        {
            var descs = new List<string>
            {
                "Ładny mebel",
                "Bardzo ładny mebel",
            };
            var furnitureList = new List<Furniture>();
            for (int i = 0; i < 49; i++)
            {
                descs.Shuffle();
                listCategory.ToList().Shuffle();
                listManufacturer.ToList().Shuffle();
                furnitureList.Add(new Furniture()
                {
                    FurnitureDescription = i + 1 + descs.First(),
                    FurnitureCost = 40 *i,
                    FurnitureName = "furniture" + i,
                    FurnitureMaterial = "material" + i,
                    FurnitureQuantity = 10 * i,
                    CategoryId = listCategory.First().CategoryId,
                    ManufacturerId = listManufacturer.First().ManufacturerId,
                });
            }

            return furnitureList;
        }

        private IEnumerable<Comment> BuildCommentList(IEnumerable<Furniture> furnitureList, IEnumerable<DAO.User> userList)
        {
            var commentList = new List<Comment>();
            var furnitureCount = furnitureList.ToList().Count;
            var counter = 0;
            var rand = new Random();
            foreach (var user in userList)
            {
                counter++;
                var furnitureId = rand.Next(furnitureCount);
                commentList.Add(new Comment
                    {
                        FurnitureId = furnitureList.ToList()[furnitureId].FurnitureId,
                        UserId = user.UserId,
                        CommentContent = "Comment" + counter,
                        CommentDate = DateTime.Now,
                    }
                );
            }

            return commentList;
        }

        private IEnumerable<Rating> BuildRatingList(IEnumerable<Furniture> furnitureList, IEnumerable<DAO.User> userList)
        {
            var ratingList = new List<Rating>();
            var furnitureCount = furnitureList.ToList().Count;
            var counter = 0;
            var rand = new Random();
            foreach (var user in userList)
            {
                counter++;
                var furnitureId = rand.Next(furnitureCount);
                ratingList.Add(new Rating()
                    {
                        FurnitureId = furnitureList.ToList()[furnitureId].FurnitureId,
                        UserId = user.UserId,
                        RatingValue =  counter % 5,
                    }
                );
            }

            return ratingList;
        }

        private IEnumerable<Order> BuildOrderList(IEnumerable<DAO.User> userList)
        {
            var orderList = new List<Order>();
            var rand = new Random();
            foreach (var user in userList)
            {
                orderList.Add(new Order()
                    {
                        UserId = user.UserId,
                        OrderDate = DateTime.Now,
                        OrderDestAdress = "Opole prószkowska 7",
                        OrderPayMethod = "Blik"
                    }
                );
            }

            return orderList;

        }

        private IEnumerable<OrderFurniture> BuildOrderFurnitureList(IEnumerable<Furniture> furnitureList,
            IEnumerable<Order> orderList)
        {
            var orderFurnitureList = new List<OrderFurniture>();
            furnitureList.ToList().Shuffle();
            orderList.ToList().Shuffle();
            var rand = new Random();
            foreach (var order in orderList)
            {
                var index = rand.Next(orderList.Count());
                orderFurnitureList.Add(new OrderFurniture()
                {
                    OrderId = orderList.ToList()[index].OrderId,
                    FurnitureId = furnitureList.ToList()[index].FurnitureId,
                });
            }

            return orderFurnitureList;
        }

    }
}
