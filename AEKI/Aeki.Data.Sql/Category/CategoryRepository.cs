using System.Threading.Tasks;
using Aeki.IData.Category;
using Microsoft.EntityFrameworkCore;

namespace Aeki.Data.Sql.Category
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AekiDbContext _context;

        public CategoryRepository(AekiDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddCategory(Domain.Category.Category category)
        {
            var categoryDAO = new DAO.Category
            {
                CategoryName = category.CategoryName,
                CategoryImgLink = category.CategoryImgLink
            };
            await _context.AddAsync(categoryDAO);
            await _context.SaveChangesAsync();
            return categoryDAO.CategoryId;
        }

        public async Task<Domain.Category.Category> GetCategory(int categoryId)
        {
            var category = await _context.Category.FirstOrDefaultAsync(x => x.CategoryId == categoryId);
            return new Domain.Category.Category(
                category.CategoryId,
                category.CategoryName,
                category.CategoryImgLink);
        }

        public async Task<Domain.Category.Category> GetCategory(string categoryName)
        {
            var category = await _context.Category.FirstOrDefaultAsync(x => x.CategoryName == categoryName);
            return new Domain.Category.Category(
                category.CategoryId,
                category.CategoryName,
                category.CategoryImgLink);
        }

        public async Task EditCategory(Domain.Category.Category category)
        {
            var editCategory = await _context.Category.FirstOrDefaultAsync(x => x.CategoryId == category.Id);
            editCategory.CategoryName = category.CategoryName;
            editCategory.CategoryImgLink = category.CategoryImgLink;
            await _context.SaveChangesAsync();
        }
    }
}