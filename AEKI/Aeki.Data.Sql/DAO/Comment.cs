﻿using System;
using Google.Protobuf;
using Org.BouncyCastle.Math.EC;

namespace Aeki.Data.Sql.DAO
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string CommentContent {get; set;}
        public DateTime CommentDate { get; set; }
        public int FurnitureId { get; set; }
        public int UserId { get; set; }
        
        public virtual Furniture Furniture { get; set; }
        public virtual User User { get; set;}

    }
}