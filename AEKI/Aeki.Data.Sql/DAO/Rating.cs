﻿namespace Aeki.Data.Sql.DAO
{
    public class Rating
    {
        public int RatingId { get; set; }
        public int RatingValue { get; set;}
        public int UserId { get; set; }
        public int FurnitureId { get; set; }
        public virtual User User { get; set;}
        public virtual Furniture Furniture { get; set;}
    }
}