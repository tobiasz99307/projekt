﻿namespace Aeki.Data.Sql.DAO
{
    public class OrderFurniture
    {
        public int OrderFurnitureId { get; set; }
        public int OrderId { get; set; }
        public int FurnitureId { get; set; }

        public virtual Order Order { get; set; }
        public virtual Furniture Furniture { get; set; }
    }
}