﻿using System.Collections;
using System.Collections.Generic;

namespace Aeki.Data.Sql.DAO
{
    public class Category
    {
        public Category()
        {
            Furnitures = new List<Furniture>();
        }
        public int CategoryId { get; set;}
        public string CategoryName { get; set;}
        public string CategoryImgLink { get; set; }
        
        public virtual ICollection<Furniture> Furnitures { get; set; }
        

    }
}