﻿using System.Collections;
using System.Collections.Generic;

namespace Aeki.Data.Sql.DAO
{
    public class Manufacturer
    {
        public Manufacturer()
        {
            Furnitures = new List<Furniture>();
        }
        
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }

        public virtual ICollection<Furniture> Furnitures { get; set; }
    }
}