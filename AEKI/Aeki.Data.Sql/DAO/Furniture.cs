﻿using System.Collections;
using System.Collections.Generic;

namespace Aeki.Data.Sql.DAO
{
    public class Furniture
    {
        public Furniture()
        {
            Comments = new List<Comment>();
            Ratings = new List<Rating>();
            OrderFurnitures = new List<OrderFurniture>();
        }

        public int FurnitureId { get; set; }
        public int FurnitureQuantity { get; set; } // dostepnosc
        public int FurnitureCost { get; set; }
        public string FurnitureName { get; set; }
        public string FurnitureMaterial { get; set; }
        public string FurnitureImgLink { get; set; }
        public string FurnitureDescription { get; set; }

        public int CategoryId { get; set; }
        public int ManufacturerId { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Category Category { get; set; }
        public  virtual ICollection<OrderFurniture> OrderFurnitures { get; set; }
    }
}