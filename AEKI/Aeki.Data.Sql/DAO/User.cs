﻿using System.Collections;
using System.Collections.Generic;

namespace Aeki.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Ratings = new List<Rating>();
            Comments = new List<Comment>();
            Orders = new List<Order>();
        }
        
        public int UserId { get; set; }
        public string UserFName { get; set; }
        public string UserLName { get; set; }
        public string UserMail { get; set;}
        public string UserPhone { get; set; }
        public bool UserVerified { get; set; }
        public bool UserCommentBanner { get; set;}
        
        
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        
        public virtual ICollection<Order> Orders { get; set; }
        
    }
}