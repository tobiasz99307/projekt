﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Aeki.Data.Sql.DAO
{
    public class Order
    {
        public Order()
        {
            Orders = new List<OrderFurniture>();
        }
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderPayMethod { get; set; }
        public string OrderDestAdress { get; set; }
        
        public int UserId { get; set; }
        public int OrderFurnitureId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<OrderFurniture> Orders { get; set; }
        

    }
}