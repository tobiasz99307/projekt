using System.Threading.Tasks;
using Aeki.IData.User;
using Microsoft.EntityFrameworkCore;
using Google.Protobuf.WellKnownTypes;

namespace Aeki.Data.Sql.User
{
    public class UserRepository: IUserRepository
    {
        
   private readonly AekiDbContext _context;

        public UserRepository(AekiDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddUser(Domain.User.User user)
        {
            var userDAO =  new DAO.User { 
                UserMail = user.Email,
                UserPhone = user.Phone,
                UserFName = user.UserFName,
                UserLName = user.UserLName,
                UserVerified = user.Verified,
                UserCommentBanner = user.UserCommentBanned
            };
            await _context.AddAsync(userDAO);
            await _context.SaveChangesAsync();
            return userDAO.UserId;
        }

        public async Task<Domain.User.User> GetUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x=>x.UserId == userId);
            return new Domain.User.User(
                user.UserId,
                user.UserFName,
                user.UserLName, 
                user.UserMail,
                user.UserPhone,
                user.UserVerified,
                user.UserCommentBanner);
        }

        public async Task<Domain.User.User> GetUser(string userMail)
        {
            var user = await _context.User.FirstOrDefaultAsync(x=>x.UserMail == userMail);
            return new Domain.User.User(
                user.UserId,
                user.UserFName,
                user.UserLName, 
                user.UserMail,
                user.UserPhone,
                user.UserVerified,
                user.UserCommentBanner);

        }

        public async Task EditUser(Domain.User.User user)
        {
            var editUser = await _context.User.FirstOrDefaultAsync(x=>x.UserId == user.Id);
            editUser.UserFName = user.UserFName;
            editUser.UserLName = user.UserLName;
            editUser.UserMail = user.Email;
            await _context.SaveChangesAsync();
        }
    }
 
}
