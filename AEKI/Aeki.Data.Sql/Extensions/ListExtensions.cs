using System;
using System.Collections.Generic;

namespace Aeki.Data.Sql.Extensions
{
    public static class ListExtensions
    {
        //metoda permutująca dane w liście
        public static void Shuffle<T>(this IList<T> list)  
        {  
            var rnd = new Random();  

            var n = list.Count;  
            while (n > 1) {  
                n--;  
                var random = rnd.Next(n + 1);
                var value = list[random];  
                list[random] = list[n];  
                list[n] = value;  
            }  
        }
 
    }
}
